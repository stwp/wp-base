export default class MenuConditionalFields {
    constructor() {
        this.fieldset = document.querySelector('.menu-theme-locations')

        if (!this.fieldset) {
            return;
        }

        this.locations = this.fieldset.querySelectorAll('input[type="checkbox"]')

        if (!this.locations || this.locations.length < 1) {
            return;
        }

        this.body = document.querySelector('body')

        this.prepareStyle()

        this.stylesContainer = this.body.querySelector('#super-menu-fields-visibility-styles')

        this.addBodyClasses()

        this.events()
    }

    prepareStyle() {
        this.body.insertAdjacentHTML('afterBegin', '<style id="super-menu-fields-visibility-styles"> </style>')
    }

    addBodyClasses() {
        let cssHide = []
        let cssShow = []

        for (let i = 0; i < this.locations.length; i++) {
            let suffix = this.locations[i]
                .getAttribute('name')
                .replace(/menu-locations\[(.*)]/gi, "$1")

            if (!this.locations[i].checked) {
                cssHide.push(`.super-menu-field-for-${suffix}`);
            } else {
                cssShow.push(`.super-menu-field-for-${suffix}`);
            }
        }

        this.stylesContainer.innerHTML = cssHide.join(',') + '{ display: none }' + cssShow.join(',') + '{ display: block }';
    }

    events() {
        for (let i = 0; i < this.locations.length; i++) {
            this.locations[i].addEventListener('change', e => {
                this.addBodyClasses()
            })
        }
    }
}
