export default class MenuIconUploader {
    constructor() {
        this.menuEditor = document.getElementById('menu-management');

        if (!this.menuEditor) {
            return;
        }

        this.events();
    }

    events() {
        this.menuEditor.addEventListener('click', event => {
            if (
                event.target.classList.contains('super-menu-image-browse') ||
                event.target.classList.contains('super-menu-image-preview')
            ) {
                event.preventDefault();

                let uploaderContainer = event.target.closest('.js-super-menu-image-container');

                if (uploaderContainer) {
                    const input = uploaderContainer.querySelector('.super-menu-image-value');
                    const button = uploaderContainer.querySelector('.super-menu-image-browse');
                    const preview = uploaderContainer.querySelector('.super-menu-image-preview');

                    if (input && button) {
                        this.modal(uploaderContainer, button, input, preview);
                    }
                }
            } else if (event.target.classList.contains('super-menu-image-remove')) {
                event.preventDefault();

                let uploaderContainer = event.target.closest('.js-super-menu-image-container');

                if (uploaderContainer) {
                    const input = uploaderContainer.querySelector('.super-menu-image-value');
                    const button = uploaderContainer.querySelector('.super-menu-image-browse');
                    const preview = uploaderContainer.querySelector('.super-menu-image-preview');

                    if (input && button) {
                        input.value = ''
                        preview.innerHTML = ''
                        uploaderContainer.classList.remove('super-menu-image-container--selected')
                    }
                }
            }
        })
    }

    modal(uploaderContainer, button, input, preview) {
        const fileFrame = wp.media.frames.file_frame = wp.media({
            title: button.dataset.title,
            button: {
                text: button.dataset.selectText
            },
            multiple: false
        });

        fileFrame.on('select', () => {
            const attachment = fileFrame.state().get('selection').first().toJSON();
            input.value = attachment.url;
            preview.innerHTML = `<img src="${attachment.url}">`

            uploaderContainer.classList.add('super-menu-image-container--selected')
        });

        fileFrame.open();
    }
}
