;(function(wp) {
  const { __ } = wp.i18n
  const { registerPlugin } = wp.plugins
  const Fragment = wp.element.Fragment
  const compose = wp.compose.compose
  const { TextControl, TextareaControl, PanelRow, PanelBody } = wp.components
  const { PluginSidebar, PluginSidebarMoreMenuItem } = wp.editPost
  const { withSelect, withDispatch } = wp.data

  const MetaBlockField = compose(
    withDispatch(function(dispatch, props) {
      return {
        setMetaFieldValue: function(value) {
          dispatch('core/editor').editPost({
            meta: { [props.fieldName]: value },
          })
        },
      }
    }),
    withSelect(function(select, props) {
      return {
        metaFieldValue: select('core/editor').getEditedPostAttribute('meta')[
          props.fieldName
        ],
      }
    }),
  )(function(props) {
    const currentValue = () => {
      return props.metaFieldValue
    }

    const updateValue = content => {
      props.setMetaFieldValue(content)
    }

    let fields = { ...window.superPostMetaboxes }
    let fieldSettings = { ...fields[props.fieldName] }

    switch (fieldSettings.type) {
      case 'textarea':
        return (
          <TextareaControl
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'number':
        return (
          <TextControl
            {...fieldSettings}
            type="number"
            value={currentValue()}
            onChange={updateValue}
          />
        )

      default:
        return (
          <TextControl
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )
    }
  })

  const Component = () => {
    let fields = { ...window.superPostMetaboxes }

    if (!fields) {
      return
    }

    let formattedFields = Object.keys(fields).map((name, index) => {
      let grid =
        fields[name].grid && fields[name].grid > 0 ? fields[name].grid : 12

      return (
        <div className={`super-builder-col--${grid}`}>
          <MetaBlockField fieldName={name} />
        </div>
      )
    })

    return (
      <Fragment>
        <PluginSidebarMoreMenuItem
          target="my-plugin-sidebar"
          icon="admin-appearance"
        >
          Customize
        </PluginSidebarMoreMenuItem>

        <PluginSidebar
          name="my-plugin-sidebar"
          icon="admin-appearance"
          title="Customize"
        >
          <PanelBody title={__('General')}>
            <PanelRow>
              <div className="super-builder-fields-container super-builder-fields-container--sidebar">
                {formattedFields}
              </div>
            </PanelRow>
          </PanelBody>
        </PluginSidebar>
      </Fragment>
    )
  }

  if (window.superPostMetaboxes) {
    registerPlugin('my-plugin-sidebar', {
      icon: 'admin-appearance',
      render: Component,
    })
  }
})(window.wp)
