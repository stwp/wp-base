const { createBlock } = wp.blocks
const { select, subscribe, dispatch } = wp.data

const { __ } = wp.i18n

export default class SuperTemplateSwitcher {
  constructor(templates) {
    this.templates = templates
    this.currentTemplate = null

    const confirmMessage = __(
      'This action will override all current data. Press OK to continue',
    )

    subscribe(() => {
      let templateName = select('core/editor').getEditedPostAttribute(
        'template',
      )

      templateName = templateName === '' ? 'default' : templateName
      this.currentTemplate = this.currentTemplate || templateName

      if (
        templateName &&
        this.currentTemplate !== templateName &&
        window.confirm(confirmMessage)
      ) {
        this.currentTemplate = templateName
        this.switchTemplate()
      }
    })
  }

  switchTemplate() {
    const { resetBlocks } = dispatch('core/editor')

    if (
      this.currentTemplate === 'default' ||
      this.currentTemplate === 'blank'
    ) {
      resetBlocks([])
      return
    }

    let templateBlocks = this.templates[this.currentTemplate]
    let currentBlocks = []

    templateBlocks &&
      templateBlocks.map(block => {
        currentBlocks.push(createBlock(block.name, block.attributes))
      })

    currentBlocks.length && resetBlocks(currentBlocks)
  }
}
