import edit from './edit'
import { globalOptions } from './global-options'
import merge from 'lodash.merge'

const { __ } = wp.i18n

const { getBlockTypes, registerBlockType, unregisterBlockType } = wp.blocks

export default class SuperSettings {
  constructor(blocks) {
    this.registerBlocks(blocks)
  }

  // deregisterBlocks() {
  //     const blocks = getBlockTypes()
  //
  //     blocks.forEach(({name}) => {
  //         name.search(/core\/block|core\/freeform|super/) < 0 &&
  //         unregisterBlockType(name)
  //     })
  // }

  registerBlocks(blocks) {
    blocks.forEach(({ settings }) => {
      const name = settings.name

      const opt = this.defaultSettings()

      merge(opt, settings)

      const attributes = { _block_name: {}, _unique_block_id: {} }
      const options = {
        ...opt.modalOptions,
        ...opt.sideOptions,
        ...globalOptions,
      }

      Object.keys(options).forEach(key => (attributes[key] = {}))

      registerBlockType(name, {
        ...opt,
        edit,
        save: () => {
          if (opt.saveCallback) {
            return opt.saveCallback()
          }

          return null
        },
        attributes,
      })
    })
  }

  defaultSettings() {
    return {
      description: '',
      icon: {
        background: '#232329',
        src: 'list-view',
        foreground: '#fff',
        size: 20,
      },
      category: 'super',
      keywords: [__('super'), __('items'), __('presentation')],
      preview: 'https://unsplash.it/260/100',
      modalOptions: {},
      sideOptions: {},
      renderCallback: false,
      supports: {
        html: false,
      },
    }
  }
}
