export const SuperFields = ({ props, options }) => {
  const { attributes, setAttributes, clientId } = props

  const {
    TextControl,
    TextareaControl,
    CheckboxControl,
    RadioControl,
    RangeControl,
    SelectControl,
    ToggleControl,
    ColorPalette,
    DateTimePicker,
    DropdownMenu,
    Dropdown,
    Button,
    FontSizePicker,
    FormTokenField,
    CodeEditor,
    Dashicon,
  } = wp.components

  const { MediaUpload, MediaPlaceholder, BlockEdit, URLInput } = wp.editor

  const { apiFetch } = wp
  const { withState } = wp.compose
  const { Fragment } = wp.element
  const { __ } = wp.i18n

  const getField = (
    name,
    settings,
    index = null,
    field = null,
    parent = null,
    parentIndex = null,
  ) => {
    const { type } = settings

    let fieldSettings = { ...settings }
    const showIf = fieldSettings.showIf
    const showOperator = fieldSettings.showOperator
    delete fieldSettings.type
    delete fieldSettings.showIf
    delete fieldSettings.showOperator

    if (showIf && showIf.length) {
      let hideField = showOperator === 'and' ? [] : false

      showIf.forEach(el => {
        Object.keys(el).forEach(key => {
          const desiredVal = el[key]
          let currentValue = null

          if (field) {
            try {
              currentValue = JSON.parse(attributes[name])
            } catch (e) {
              currentValue = attributes[name]
            }
          } else {
            currentValue = attributes[key]
          }

          if (
            field &&
            parent &&
            currentValue &&
            currentValue[parentIndex] &&
            currentValue[parentIndex][parent][index]
          ) {
            currentValue = currentValue[parentIndex][parent][index][key]
          } else if (field && currentValue && currentValue[index]) {
            currentValue = currentValue[index][key]
          }

          if (!currentValue && attributes[key]) {
            currentValue = attributes[key]
          }

          if (showOperator === 'and') {
            hideField.push(currentValue === desiredVal)
          } else if (currentValue === desiredVal) {
            hideField = true
          }
        })
      })

      if (showOperator === 'and') {
        hideField = hideField.indexOf(false) < 0
      }

      if (!hideField) {
        return
      }
    }

    const makeObj = value => {
      return value && typeof value === 'string' ? JSON.parse(value) : value
    }

    const updateValue = value => {
      let currentValue = value

      if (index !== null && field !== null) {
        currentValue = makeObj(attributes[name]) || []

        if (parent) {
          currentValue[parentIndex][parent] = makeObj(
            currentValue[parentIndex][parent],
          )
          currentValue[parentIndex][parent][index] = {
            ...currentValue[parentIndex][parent][index],
            [field]: value,
          }
        } else {
          currentValue[index] = { ...currentValue[index], [field]: value }
        }

        currentValue = JSON.stringify(currentValue)
      }

      return setAttributes({ [name]: currentValue })
    }

    const currentValue = () => {
      let currentValue = attributes[name]

      if (index !== null && field !== null) {
        currentValue = makeObj(currentValue)

        const hasValue =
          currentValue &&
          ((currentValue[index] && currentValue[index][field]) ||
            (parent &&
              currentValue[parentIndex] &&
              currentValue[parentIndex][parent]))

        return hasValue
          ? parent
            ? makeObj(currentValue[parentIndex][parent])[index][field]
            : currentValue[index][field]
          : ''
      }

      return currentValue
    }

    switch (type) {
      case 'text':
        return (
          <TextControl
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'number':
        return (
          <TextControl
            {...fieldSettings}
            type="number"
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'url':
        return (
          <Fragment>
            <div class="components-base-control__field">
              {fieldSettings.label ? (
                <label className="components-base-control__label">
                  {fieldSettings.label}
                </label>
              ) : (
                ''
              )}
              <URLInput value={currentValue()} onChange={updateValue} />
              {fieldSettings.help ? (
                <p className="components-base-control__help">
                  {fieldSettings.help}
                </p>
              ) : (
                ''
              )}
            </div>
          </Fragment>
        )

      case 'textarea':
        return (
          <TextareaControl
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'checkbox':
        return (
          <CheckboxControl
            {...fieldSettings}
            checked={currentValue()}
            onChange={updateValue}
          />
        )

      case 'radio':
        return (
          <RadioControl
            {...fieldSettings}
            selected={currentValue()}
            onChange={updateValue}
          />
        )

      case 'range':
        return (
          <RangeControl
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'select':
        if (
          typeof fieldSettings.options === 'object' &&
          fieldSettings.options[0].value !== ''
        ) {
          fieldSettings.options.unshift({
            label: __('--- Select Option ---'),
            value: '',
          })
        }

        return (
          <SelectControl
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'toggle':
        return (
          <ToggleControl
            {...fieldSettings}
            checked={currentValue()}
            onChange={() => updateValue(!currentValue())}
          />
        )

      case 'color':
        return (
          <div className="components-base-control__field components-base-control__color-field">
            {fieldSettings.label ? (
              <label className="components-base-control__label">
                {fieldSettings.label}
              </label>
            ) : (
              ''
            )}
            <span style={currentValue() ? {
              display: 'inline-block',
              width: '20px',
              height: '14px',
              verticalAlign: 'middle',
              margin: '0 0 0 8px',
              boxShadow: 'inset 0 0 3px 0px #9c9c9c',
              backgroundColor: currentValue()
            } : {}}> </span>
            <div>
              <ColorPalette
                {...fieldSettings}
                value={currentValue()}
                onChange={updateValue}
              />
            </div>
          </div>
        )

      case 'date':
        return (
          <DateTimePicker
            {...fieldSettings}
            currentDate={currentValue() || new Date()}
            onChange={updateValue}
          />
        )

      case 'dropMenu':
        return <DropdownMenu {...fieldSettings} />

      case 'dropdown':
        return (
          <Dropdown
            {...fieldSettings}
            renderToggle={({ isOpen, onToggle }) => (
              <Button isPrimary onClick={onToggle} aria-expanded={isOpen}>
                Toggle Popover!
              </Button>
            )}
            renderContent={() => <div>Content</div>}
          />
        )

      case 'fontSizePicker':
        return (
          <FontSizePicker
            {...fieldSettings}
            value={currentValue()}
            onChange={updateValue}
          />
        )

      case 'heading':
        const HeadingTag = fieldSettings.tag ? fieldSettings.tag : 'h4'

        return (
          <HeadingTag className="super-builder-heading">
            {fieldSettings.label}
          </HeadingTag>
        )

      case 'postPicker':
        const { postType } = fieldSettings
        delete fieldSettings.postTypeect

        let route = `/wp/v2/${postType}`
        let isCategory = false

        const resolveValue = () => {
          return (currentValue() || [])
            .map(el => (typeof el === 'object' && el.title ? el.title : ''))
            .filter(el => el)
        }

        if (postType === 'categories') {
          route = '/wp/v2/categories'
          isCategory = true
        }

        const PostSuggestions = withState({
          tokens: resolveValue(),
          ids: [],
          suggestions: [],
        })(({ ids, tokens, suggestions, setState }) => {
          const fetchPosts = search => {
            let current_route =
              search.length > 0
                ? route + `?orderby=relevance&search=${search}`
                : route

            apiFetch({
              path: current_route,
              search,
            }).then(results => {
              setState({ ids: results.map(el => el.id) })
              setState({
                suggestions: results.map(el =>
                  isCategory ? el.name : el.title.rendered,
                ),
              })
            })
          }

          const changeTokens = tokens => {
            setState({ tokens })

            if (tokens.length > suggestions.length) {
              const newValue = currentValue()

              updateValue(newValue.filter(el => tokens.indexOf(el.title) > -1))
            } else {
              updateValue(
                tokens
                  .map(el => {
                    if (suggestions && suggestions.indexOf(el) > -1) {
                      return {
                        title: el,
                        id: ids[suggestions.indexOf(el)],
                      }
                    }

                    return null
                  })
                  .filter(el => el),
              )
            }
          }

          return (
            <FormTokenField
              {...fieldSettings}
              suggestions={suggestions}
              value={tokens}
              onChange={changeTokens}
              onInputChange={fetchPosts}
            />
          )
        })

        return <PostSuggestions />

      case 'image':
        const currentImages = currentValue()
        const isGallery =
          fieldSettings.multiple && currentImages && 'length' in currentImages
        const hasImages =
          typeof currentImages === 'object' &&
          (isGallery
            ? currentImages.length
            : currentImages && 'id' in currentImages)

        const imageCallback = (item, index) => {
          const ImagePreview = withState({
            item: item,
          })(({ item, setState }) => {
            if (item && !item.url) {
              const getImage = new Promise(resolve => {
                wp.data.select('core').getMedia(item.id, true)
                setTimeout(() => {
                  resolve(wp.data.select('core').getMedia(item.id, true))
                }, 1000)
              })

              getImage.then(imageObj => {
                if (imageObj && imageObj.source_url) {
                  item.url = imageObj.source_url
                  item.alt = imageObj.alt_text
                  setState({ item })
                }
              })
            }

            return (
              <div className="super-image-preview__item">
                <div
                  className="super-image-preview__item--close"
                  onClick={e => removeImages(e, index)}
                >
                  <Dashicon icon="no-alt" />
                </div>
                <img src={item.url} alt={item.alt} />
              </div>
            )
          })

          return <ImagePreview />
        }

        const EditLink = ({ label }) => {
          const mediaSettings = isGallery
            ? {
                multiple: true,
                gallery: true,
                value: hasImages ? currentImages.map(img => img.id) : [],
              }
            : {
                value: currentImages.id,
              }

          return (
            <MediaUpload
              onSelect={onSelect}
              type="image"
              {...mediaSettings}
              render={({ open }) => (
                <div className="super-image-preview__cta">
                  <a href="#" onClick={open}>
                    {label}
                  </a>
                </div>
              )}
            />
          )
        }

        const onSelect = media => {
          if (typeof media !== 'object') {
            return
          }

          if (fieldSettings.multiple) {
            const mediaData = []

            media.forEach(({ id, url, link, caption, alt }) => {
              mediaData.push({ id, url, link, caption, alt })
            })

            updateValue(mediaData)
            return
          }

          const { id, url, link, caption, alt } = media
          updateValue({ id, url, link, caption, alt })
        }

        const removeImages = (e, index = 0) => {
          e.preventDefault()

          const activeImages = isGallery
            ? attributes[name].filter((el, key) => index !== key)
            : {}

          updateValue(activeImages)
        }

        const Image = ({ items }) =>
          isGallery ? items.map(imageCallback) : imageCallback(items, 0)

        const defaultSettings = {
          labels: {
            title: __('Title'),
            name: __('Name'),
          },
          icon: 'format-image',
          allowedTypes: ['image'],
        }

        fieldSettings = Object.assign({}, defaultSettings, fieldSettings)

        return (
          <Fragment>
            {!!hasImages && (
              <div>
                <label className="components-base-control__label">
                  {fieldSettings.labels.title}
                </label>
                <div className="super-image-preview">
                  <Image items={currentImages} />
                  <EditLink
                    label={isGallery ? __('Edit gallery') : __('Edit image')}
                  />
                </div>
              </div>
            )}
            {!hasImages && (
              <MediaPlaceholder
                {...fieldSettings}
                accept="image/*"
                type="image"
                onSelect={onSelect}
              />
            )}
          </Fragment>
        )

      case 'editor':
        const editorId =
          clientId +
          name +
          (field || '') +
          (index || '') +
          (parent || '') +
          (parentIndex || '')
        const editorCallback = state => {
          return state.content !== undefined
            ? updateValue(state.content)
            : setAttributes(state)
        }

        const SuperEditor = withState({
          showCode: false,
        })(({ showCode, setState }) => {
          return (
            <div class="components-base-control__field">
              {fieldSettings.label ? (
                <label className="components-base-control__label">
                  {fieldSettings.label}
                </label>
              ) : (
                ''
              )}
              <ToggleControl
                label={__('Code Editor')}
                checked={showCode}
                onChange={() => setState({ showCode: !showCode })}
              />

              {showCode ? (
                <CodeEditor value={currentValue()} onChange={updateValue} />
              ) : (
                <BlockEdit
                  name="core/freeform"
                  attributes={{ content: currentValue() }}
                  clientId={editorId}
                  setAttributes={editorCallback}
                />
              )}
              {fieldSettings.help ? (
                <p className="components-base-control__help">
                  {fieldSettings.help}
                </p>
              ) : (
                ''
              )}
            </div>
          )
        })

        return <SuperEditor />

      case 'generator':
        let items = makeObj(currentValue()) || []
        const fields = fieldSettings.fields

        const fieldsBlock = (el, indexBlock) => (
          <div
            className={
              'super-generator--item' + (el['_toggled'] ? ' toggled' : '')
            }
          >
            <div className="super-generator--tools">
              <div
                className="super-generator--move-up"
                onClick={e => moveField(e, indexBlock, 'up')}
              >
                <Dashicon icon="arrow-up-alt" />
              </div>
              <div
                className="super-generator--move-down"
                onClick={e => moveField(e, indexBlock, 'down')}
              >
                <Dashicon icon="arrow-down-alt" />
              </div>
              <div
                className="super-generator--toggle"
                onClick={e => toggleField(e, indexBlock)}
              >
                <Dashicon icon="minus" />
              </div>
              <div
                className="super-generator--clone"
                onClick={e => cloneField(e, indexBlock)}
              >
                <Dashicon icon="admin-page" />
              </div>
              <div
                className="super-generator--remove"
                onClick={e => removeFieldsBlock(e, indexBlock)}
              >
                <Dashicon icon="no-alt" />
              </div>
            </div>
            <div className="super-builder-fields-container">
              {fields &&
                Object.keys(fields).map(key => {
                  let saveField = getField(
                    name,
                    fields[key],
                    indexBlock,
                    key,
                    field,
                    index,
                  )
                  let currFieldSettings = { ...fields[key] }
                  let grid =
                    currFieldSettings.grid && currFieldSettings.grid > 0
                      ? currFieldSettings.grid
                      : 12

                  return (
                    <div className={`super-builder-col--${grid}`}>
                      {saveField}
                    </div>
                  )
                })}
            </div>
          </div>
        )

        delete fieldSettings.fields

        const addFieldsBlock = e => {
          e.preventDefault()

          items.push({})
          updateValue(JSON.stringify(items))
        }

        const removeFieldsBlock = (e, index) => {
          e.preventDefault()

          items.splice(index, 1)
          updateValue(JSON.stringify(items))
        }

        const moveField = (e, index, dir) => {
          e.preventDefault()

          const nextIndex = index + (dir === 'up' ? -1 : 1)

          if (nextIndex > -1 && nextIndex < items.length) {
            items.splice(nextIndex, 0, items.splice(index, 1)[0])
            updateValue(JSON.stringify(items))
          }
        }

        const toggleField = (e, index) => {
          e.preventDefault()

          if (items[index]) {
            items[index]['_toggled'] = !items[index]['_toggled']
          }

          updateValue(JSON.stringify(items))
        }

        const cloneField = (e, index) => {
          e.preventDefault()

          const newEl = items[index]

          items.splice(index + 1, 0, newEl)
          updateValue(JSON.stringify(items))
        }

        return (
          <Fragment>
            <div class="components-base-control__field">
              {fieldSettings.label ? (
                <label className="components-base-control__label">
                  {fieldSettings.label}
                </label>
              ) : (
                ''
              )}

              <div className="super-generator--wrapper">
                {items.map(fieldsBlock)}

                <Button isPrimary onClick={addFieldsBlock}>
                  <Dashicon icon="plus" />
                  {__('Add item')}
                </Button>
              </div>

              {fieldSettings.help ? (
                <p className="components-base-control__help">
                  {fieldSettings.help}
                </p>
              ) : (
                ''
              )}
            </div>
          </Fragment>
        )
    }
  }

  return Object.keys(options).map(key => {
    let field = getField(key, options[key])
    let currFieldSettings = { ...options[key] }
    let grid =
      currFieldSettings.grid && currFieldSettings.grid > 0
        ? currFieldSettings.grid
        : 12

    return <div className={`super-builder-col--${grid}`}>{field}</div>
  })
}
