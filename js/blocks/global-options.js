const {__} = wp.i18n

let containerWidthOptions = [
    {label: __('Default'), value: ''},
    {label: __('Full'), value: 'full'},
    {label: __('Wide'), value: 'wide'},
    {label: __('Normal'), value: 'normal'},
    {label: __('Narrow'), value: 'narrow'},
    {label: __('Extra Narrow'), value: 'extra-narrow'},
    {label: __('RAW(100%, no spacing)'), value: 'raw'},
]

let verticalSpaceOptions = [
    {label: __('Default'), value: ''},
    {label: __('Extra large'), value: 'extra-large'},
    {label: __('Large'), value: 'large'},
    {label: __('Normal'), value: 'normal'},
    {label: __('Minimal'), value: 'minimal'},
    {label: __('Extra minimal'), value: 'extra-minimal'},
    {label: __('None'), value: 'none'},
    {label: __('Custom'), value: 'custom'},
]

let cssUnits = [
    {label: __('px'), value: ''},
    {label: __('%'), value: '%'},
    {label: __('vw'), value: 'vw'},
    {label: __('vh'), value: 'vh'},
    {label: __('rem'), value: 'rem'},
    {label: __('em'), value: 'em'},
]

let _backgroundColorOptions = [
    {name: 'Black', color: '#414550'},
    {name: 'White', color: '#fff'},
]

let shapesOptions = [
    {label: 'None', value: ''},
    // {label: 'arrow', value: 'arrow'},
    // {label: 'arrow-negative', value: 'arrow-negative'},
    // {label: 'book', value: 'book'},
    // {label: 'book-negative', value: 'book-negative'},
    // {label: 'clouds', value: 'clouds'},
    // {label: 'clouds-negative', value: 'clouds-negative'},
    {label: 'Curve', value: 'curve'},
    {label: 'Curve asymmetrical', value: 'curve-asymmetrical'},
    {label: 'Curve asymmetrical negative', value: 'curve-asymmetrical-negative'},
    {label: 'Curve negative', value: 'curve-negative'},
    // {label: 'drops', value: 'drops'},
    // {label: 'drops-negative', value: 'drops-negative'},
    {label: 'Mountains', value: 'mountains'},
    // {label: 'opacity-fan', value: 'opacity-fan'},
    // {label: 'opacity-tilt', value: 'opacity-tilt'},
    // {label: 'pyramids', value: 'pyramids'},
    // {label: 'pyramids-negative', value: 'pyramids-negative'},
    // {label: 'split', value: 'split'},
    // {label: 'split-negative', value: 'split-negative'},
    // {label: 'tilt', value: 'tilt'},
    // {label: 'triangle', value: 'triangle'},
    // {label: 'triangle-asymmetrical', value: 'triangle-asymmetrical'},
    // {label: 'triangle-asymmetrical-negative', value: 'triangle-asymmetrical-negative'},
    // {label: 'triangle-negative', value: 'triangle-negative'},
    {label: 'Wave brush', value: 'wave-brush'},
    {label: 'Waves', value: 'waves'},
    {label: 'Waves negative', value: 'waves-negative'},
    // {label: 'waves-pattern', value: 'waves-pattern'},
    // {label: 'zigzag', value: 'zigzag'},
]

export const globalOptions = {
    _blockWidth: {
        type: 'select',
        label: __('Section Width'),
        options: containerWidthOptions,
    },
    _topSpace: {
        type: 'select',
        label: __('Top space'),
        options: verticalSpaceOptions,
        grid: 5,
    },
    _customTopSpace: {
        type: 'number',
        label: __('Value'),
        min: 1,
        max: 300,
        step: 1,
        grid: 3,
        showIf: [
            {_topSpace: 'custom'},
        ]
    },
    _customTopSpaceUnit: {
        type: 'select',
        label: __('Unit'),
        options: cssUnits,
        grid: 4,
        showIf: [
            {_topSpace: 'custom'},
        ]
    },
    _bottomSpace: {
        type: 'select',
        label: __('Bottom space'),
        options: verticalSpaceOptions,
        grid: 5,
    },
    _customBottomSpace: {
        type: 'number',
        label: __('Value'),
        min: 0,
        max: 300,
        step: 1,
        grid: 3,
        showIf: [
            {_bottomSpace: 'custom'},
        ],
    },
    _customBottomSpaceUnit: {
        type: 'select',
        label: __('Unit'),
        options: cssUnits,
        grid: 4,
        showIf: [
            {_bottomSpace: 'custom'},
        ],
    },
    _backgroundImage: {
        type: 'image',
        labels: {
            title: __('Background image'),
        },
        help: __('This image will be used as a background image for this block'),
        icon: 'format-image',
        multiple: false,
        section: __('Appearance'),
    },
    _backgroundPosition: {
        type: 'select',
        label: __('Background position'),
        options: [
            {label: __('Top'), value: 'center top'},
            {label: __('Center'), value: 'center center'},
            {label: __('Bottom'), value: 'center bottom'},
            {label: __('Custom'), value: 'custom'},
        ],
        section: __('Appearance'),
        grid: 7
    },
    _customBackgroundPosition: {
        type: 'text',
        label: __('Position'),
        help: __('e.g. `50% 80%`'),
        showIf: [
            {_backgroundPosition: 'custom'}
        ],
        section: __('Appearance'),
        grid: 5
    },
    _backgroundSize: {
        type: 'select',
        label: __('Background size'),
        options: [
            {label: __('Cover'), value: 'cover'},
            {label: __('Contain'), value: 'contain'},
            {label: __('Auto'), value: 'auto'},
            {label: __('Custom'), value: 'custom'},
        ],
        section: __('Appearance'),
        grid: 7
    },
    _customBackgroundSize: {
        type: 'text',
        label: __('Size'),
        help: __('e.g. `auto 85%`'),
        showIf: [
            {_backgroundSize: 'custom'}
        ],
        section: __('Appearance'),
        grid: 5
    },
    _backgroundColor: {
        type: 'color',
        label: __('Background color'),
        //disableCustomColors: true,
        colors: _backgroundColorOptions,
        section: __('Appearance')
    },
    _textColor: {
        type: 'color',
        label: __('Text color'),
        //disableCustomColors: true,
        colors: _backgroundColorOptions,
        section: __('Appearance')
    },
    _topShape: {
        type: 'select',
        label: __('Shape'),
        options: shapesOptions,
        section: __('Top Shape')
    },
    _topShapeZoom: {
        type: 'range',
        label: __('Zoom'),
        min: 100,
        max: 300,
        step: 1,
        section: __('Top Shape')
    },
    _topShapeHeight: {
        type: 'range',
        label: __('Max Height'),
        min: 1,
        max: 500,
        step: 1,
        section: __('Top Shape')
    },
    _topShapeBackgroundColor: {
        type: 'color',
        label: __('Background color'),
        //disableCustomColors: true,
        colors: _backgroundColorOptions,
        section: __('Top Shape')
    },
    _bottomShape: {
        type: 'select',
        label: __('Shape'),
        options: shapesOptions,
        section: __('Bottom Shape')
    },
    _bottomShapeZoom: {
        type: 'range',
        label: __('Zoom'),
        min: 100,
        max: 300,
        step: 1,
        section: __('Bottom Shape')
    },
    _bottomShapeHeight: {
        type: 'range',
        label: __('Max Height'),
        min: 1,
        max: 500,
        step: 1,
        section: __('Bottom Shape')
    },
    _bottomShapeBackgroundColor: {
        type: 'color',
        label: __('Background color'),
        //disableCustomColors: true,
        colors: _backgroundColorOptions,
        section: __('Bottom Shape')
    },
    _blockId: {
        type: 'text',
        label: __('Block ID'),
        help: __('Example: `testimonials`'),
        section: __('Other')
    },
    _regenerate_unique_block_id: {
        type: 'checkbox',
        label: __('Regenerate Unique Block ID'),
        section: __('Other'),
    },
}
