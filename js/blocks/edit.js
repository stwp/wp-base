import {SuperFields} from './fields'
import {globalOptions} from './global-options'

const {Fragment, Component} = wp.element
const {Dashicon, Modal, Popover, PanelBody} = wp.components
const {__} = wp.i18n

const {InspectorControls, BlockControls} = wp.editor

export default class SuperEdit extends Component {
    constructor(props) {
        super(props)

        this.state = {
            modalOpen: false,
            previewShow: false,
        }

        this.openModal = this.openModal.bind(this)
        this.showPreview = this.showPreview.bind(this)
    }

    componentDidMount() {
        const {name, attributes} = this.props
        const {modalOptions, sideOptions} = wp.blocks.getBlockType(name)
        const options = {...modalOptions, ...sideOptions, ...globalOptions}

        Object.keys(options).forEach(
            key => !attributes[key] && (attributes[key] = ''),
        )

        !attributes['_block_name'] && (attributes['_block_name'] = name)

        if (!attributes['_unique_block_id'] || attributes['_regenerate_unique_block_id']) {
            attributes['_regenerate_unique_block_id'] = false
            attributes['_unique_block_id'] = Math.random().toString(36).substr(2, 9)
        }
    }

    openModal(state) {
        this.setState({modalOpen: state})
    }

    showPreview(state) {
        this.setState({previewShow: state})
    }

    render() {
        const {name} = this.props

        const {
            title,
            description,
            preview,
            icon,
            modalOptions,
            sideOptions,
            supports,
            renderCallback,
        } = wp.blocks.getBlockType(name)

        let {modalOpen, previewShow} = this.state

        // Remove the
        let blockOptions = {...globalOptions}
        let supportsKeys = Object.keys(supports)

        for (let i = 0, length = supportsKeys.length; i < length; i++) {
            let key = supportsKeys[i]

            if (supports[key] === false || sideOptions[key] || modalOptions[key]) {
                delete blockOptions[key]
            }
        }

        // Remove the old global option if it is already redeclared in sidebar or modal.
        let globalKeys = Object.keys(blockOptions)

        for (let i = 0, length = globalKeys.length; i < length; i++) {
            let key = globalKeys[i]

            if (sideOptions[key] || modalOptions[key]) {
                delete blockOptions[key]
            }
        }

        let haveModalOptions = modalOptions && Object.keys(modalOptions).length

        let sectionedBlockOptions = {
            "General": {}
        };

        Object.keys(blockOptions).forEach((key) => {
            if (blockOptions[key].section) {
                if (!sectionedBlockOptions[blockOptions[key].section]) {
                    sectionedBlockOptions[blockOptions[key].section] = {}
                }

                sectionedBlockOptions[blockOptions[key].section][key] = blockOptions[key]
            } else {
                sectionedBlockOptions['General'][key] = blockOptions[key]
            }
        });

        const blockSidebarSections = blockOptions && Object.keys(blockOptions).length ? (
            Object.keys(sectionedBlockOptions).map((key) => {
                return (
                    <PanelBody title={key} initialOpen={'General' === key}>
                        <div className="super-builder-fields-container super-builder-fields-container--sidebar">
                            <SuperFields props={this.props} options={sectionedBlockOptions[key]}/>
                        </div>
                    </PanelBody>
                )
            })
        ) : (
            ''
        )

        return (
            <Fragment>
                <div className="super-block" onDoubleClick={() => this.openModal(true)}>
                    {renderCallback ? (
                        renderCallback(this)
                    ) : (
                        <div className="super-block-inner">
                            <div className="super-block__info">
                                <h2>{title}</h2>
                                <p>{description}</p>
                            </div>
                        </div>
                    )}
                    <div className="super-block__cta">
                        {haveModalOptions ? (
                            <button onClick={() => this.openModal(true)}>
                                <Dashicon icon="edit"/>
                            </button>
                        ) : (
                            ''
                        )}
                    </div>
                </div>
                <InspectorControls>
                    {blockSidebarSections}

                    {sideOptions && Object.keys(sideOptions).length ? (
                        <PanelBody title={__('Extra Options')}>
                            <div className="super-builder-fields-container super-builder-fields-container--sidebar">
                                <SuperFields props={this.props} options={sideOptions}/>
                            </div>
                        </PanelBody>
                    ) : (
                        ''
                    )}
                </InspectorControls>
                <BlockControls/>
                {haveModalOptions ? (
                    <Fragment>
                        {modalOpen && (
                            <Modal
                                icon={<Dashicon icon="admin-generic"/>}
                                title={title + __(' Options')}
                                onRequestClose={e => {
                                    if (
                                        (e.srcElement &&
                                            e.srcElement.classList.contains(
                                                'components-modal__screen-overlay',
                                            )) ||
                                        (e.target &&
                                            e.target.classList.contains('components-button'))
                                    ) {
                                        this.openModal(false)
                                        //wp.data.dispatch( 'core/editor' ).savePost()
                                    }
                                }}
                            >
                                <div className="super-builder-fields-container">
                                    <SuperFields props={this.props} options={modalOptions}/>
                                </div>
                            </Modal>
                        )}
                    </Fragment>
                ) : (
                    ''
                )}
            </Fragment>
        )
    }
}
