import Util from './Util.js'

class Menu {
  constructor($id) {
    this._opt = {
      nav_active_class: 'general-navigation--active',
      mobile_menu_active_class: 'general-menu-clone--active',
      overlay_active_class: 'menu-overlay--active',
      unique_id: Math.random().toString(36).substr(2, 9)
    }

    this._id = $id
    this._nav = document.getElementById($id)

    if(! this._nav){
      return;
    }

    this._menu = this._nav.getElementsByClassName('menu')[0]
    this._button = this._nav.getElementsByClassName('navigation-button')[0]
    this._mobile_menu = null
    this._menu_overlay = null
    this._body = document.getElementsByTagName('body')[0]

    this.init()
  }

  init() {
    this.arrangeSubmenusInitial()
    this.arrangeSubmenusDynamically()

    this.createMobileMenu()

    this._button.addEventListener('click', () => {
      this.toggleMenu()
    })

    this._menu_overlay.addEventListener('click', () => {
      this.closeMenu()
    })

    Util.on('click', this._menu_overlay, (event, el) => {
      this.closeMenu()
    })

    let mobileMenuAnchors = this._mobile_menu.querySelectorAll('a')

    Util.on('click', mobileMenuAnchors, (event, el) => {
      this.closeMenu()
    })

    Util.on('click', this._menu.getElementsByTagName('a'), (event, el) => {
      if(el.getAttribute('href') === '#'){
        event.preventDefault();
        return false;
      }
    })
  }

  createMobileMenu() {
    if (!this._mobile_menu) {
      let clone = this._menu.cloneNode(true)

      clone.id = 'js-general-menu-clone-' + this._opt.unique_id
      Util.addClass(clone, 'general-menu-clone')

      this._body.appendChild(clone)

      this._mobile_menu = document.getElementById('js-general-menu-clone-' + this._opt.unique_id)
    }

    if (!this._menu_overlay) {
      let div = document.createElement('div')
      div.id = 'js-menu-overlay'
      div.classList.add('menu-overlay')
      this._body.appendChild(div)
      this._menu_overlay = document.getElementById('js-menu-overlay')
    }
  }

  /*
    -------------------------------------------------------------------------------
    Submenus
    -------------------------------------------------------------------------------
    */
  arrangeSubmenusInitial() {
    let submenus = this._nav.getElementsByClassName('menu--sub-menu')

    // TODO: Arrange submenus in viewport.
    // this.arrangeSubmenus(submenus)
  }

  arrangeSubmenus(submenus) {
    Util.each(submenus, el => {
      this.addInViewport(el)
    })
  }

  arrangeSubmenusDynamically() {
    let li = this._nav.getElementsByClassName('menu__item')

    Util.each(li, el => {
      el.addEventListener('mouseenter', () => {
        let submenus = el.getElementsByClassName('menu--sub-menu')
        this.arrangeSubmenus(submenus)
      })
    })
  }

  addInViewport(el) {
    if (!this.isInViewport(el)) {
      Util.addClass(el, 'position-in-viewport')
    }
  }

  isInViewport(el) {
    let bounding = el.getBoundingClientRect()

    return (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.bottom <=
        (window.innerHeight || document.documentElement.clientHeight) &&
      bounding.right <=
        (window.innerWidth || document.documentElement.clientWidth)
    )
  }

  /*
    -------------------------------------------------------------------------------
    Opening the mobile menu
    -------------------------------------------------------------------------------
    */
  openMenu() {
    Util.addClass(this._nav, this._opt.nav_active_class)
    Util.addClass(this._mobile_menu, this._opt.mobile_menu_active_class)
    Util.addClass(this._menu_overlay, this._opt.overlay_active_class)
    this._body.style.overflow = 'hidden'
  }

  closeMenu() {
    Util.removeClass(this._nav, this._opt.nav_active_class)
    Util.removeClass(this._mobile_menu, this._opt.mobile_menu_active_class)
    Util.removeClass(this._menu_overlay, this._opt.overlay_active_class)
    this._body.style.overflow = ''
  }

  toggleMenu() {
    if (this.menuIsOpen()) {
      this.closeMenu()
      return
    }

    this.openMenu()
  }

  menuIsOpen() {
    return Util.hasClass(this._nav, this._opt.nav_active_class)
  }
}

export default Menu
