superBlockIsActive = (blockName) => {

    if(! superMainData){
        throw new Error('superMainData object does not exists under global window namespace. Check "Config.php" from "wp-framework" module')
    }

    return Object.values(superMainData.active_blocks).indexOf(`super/${blockName}`) >= 0
}
