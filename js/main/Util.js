class Util {
  static addClass(el, className) {
    el.classList.add(className)
  }

  static removeClass(el, className) {
    el.classList.remove(className)
  }

  static hasClass(el, className) {
    return el.classList.contains(className)
  }

  static on(event, elements, callback) {
    if (elements instanceof Array) {
      Util.each(elements, el => {
        Util._on(event, el, callback)
      })
    } else {
      Util._on(event, elements, callback)
    }
  }

  static _on(event, elements, callback) {
    if (elements && elements.length > 0) {
      for (let i = 0, length = elements.length; i < length; i++) {
        elements[i].addEventListener(event, e => {
          callback(e, elements[i])
        })
      }
    } else if (elements instanceof Element) {
      elements.addEventListener(event, e => {
        callback(e, elements)
      })
    }
  }

  static each(elements, callback) {
    if (elements && elements.length > 0) {
      for (let i = 0, length = elements.length; i < length; i++) {
        callback(elements[i], i)
      }
    } else if (elements instanceof Element) {
      callback(elements, 0)
    }
  }

  static requestInterval(fn, delay) {
    let start = new Date().getTime()
    const raf =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      (cb => setInterval(cb, 1000 / 60))

    raf(function loop() {
      const current = new Date().getTime()
      let result = null

      if (current - start >= delay) {
        result = fn.call()
        start = current
      }

      !result && raf(loop)
    })
  }
}

export default Util
